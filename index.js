const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute");

// Server
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// MongoDB
mongoose.connect(
"mongodb+srv://dbjuliussaul:zxc_3000@wdc028-course-booking.6rxug.mongodb.net/b138_to-do?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
}
);

// task route
app.use("/tasks", taskRoute);


app.listen(port, () => console.log(`Now listening to port ${port}`));
