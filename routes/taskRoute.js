// Setup express dependency
const express = require("express");
// Create a Router instance
const router = express.Router();
// import TaskController.js
const taskController = require("../controllers/taskController");

// Routes

// GET Route
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

// POST Route
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// DELETE Route
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// PUT Route
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));;
})



// Create route for getting a specific task
router.get("/:id", (req, res) => {
	taskController.getSpecificTask(req.params.id)
	// Return result to postman
	.then(resultFromController => res.send(resultFromController));
})

// Change status to "complete"
router.put("/:id/:status", (req, res) => {
	taskController.updateStatusTask(req.params.id, req.params.status).then(resultFromController => res.send(resultFromController));;
})

module.exports = router;
