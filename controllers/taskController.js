const Task = require("../models/task");

// GET function
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

// POST function
module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if (error) {
			console.log(error);
			return false;
		}
		else {
			return task;
		}
	})
}

// DELETE function
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if (err) {
			console.log(err);
			return false;
		}
		else {
			return removedTask;
		}
	})
}

// PUT function
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if (error) {
			console.log(error);
			return false;
		}

		result.name = newContent.name;

		return result.save().then((updateTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
					return false;
				}
			else {
				return updateTask;
			}
		})
	})
}




// Create a controller function for creating a specific task
module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	})
}

// Create a controller function for making status to "complete"
module.exports.updateStatusTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if (error) {
			console.log(error);
			return false;
		}

		result.status = (newContent.status = newContent);

		return result.save().then((updateTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
					return false;
				}
			else {
				return updateTask;
			}
		})
	})
}
